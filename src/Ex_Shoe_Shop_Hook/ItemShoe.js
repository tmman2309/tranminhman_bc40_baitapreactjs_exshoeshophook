import React from "react";

export default function ItemShoe({ item, handleAddToCart, handleModal }) {
  let { id ,image, name, price } = item;

  return (
    <div className="col-3 p-4">
      <div className="card">
        <a
          onClick={() => {
            {
              handleModal(id);
            }
          }}
          href="#"
          data-toggle="modal"
          data-target="#exampleModal"
        >
          <img className="card-img-top" src={image} />
        </a>
        <div className="card-body">
          <h5 className="card-title">{name}</h5>
          <p className="card-text">{price}</p>
          <a
            onClick={() => {
              handleAddToCart(item);
            }}
            href="#"
            className="btn btn-primary"
          >
            Add To Card
          </a>
        </div>
      </div>
    </div>
  );
}
