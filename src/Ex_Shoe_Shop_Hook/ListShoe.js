import React from "react";
import ItemShoe from "./ItemShoe";

export default function ListShoe({ listShoe, handleAddToCart, handleModal }) {
  return (
    <div className="row">
      {listShoe.map((item) => {
        return (
          <ItemShoe
            key={item.id}
            handleAddToCart={handleAddToCart}
            handleModal={handleModal}
            item={item}
          />
        );
      })}
    </div>
  );
}
