import React, { useState } from "react";
import ListShoe from "./ListShoe";
import { dataShoe } from "./dataShoe";
import Cart from "./Cart";
import Modal from "./Modal";

export default function Ex_Shoe_Shop_Hook() {
  const [listShoe, setListShoe] = useState(dataShoe);
  const [cart, setCart] = useState([]);
  const [content, setContent] = useState({});

  // handleAddToCart
  let handleAddToCart = (shoe) => {
    let cloneCart = [...cart];
    let index = cloneCart.findIndex((item) => {
      return item.id == shoe.id;
    });

    if (index == -1) {
      let newShoe = { ...shoe, soLuong: 1 };
      cloneCart.push(newShoe);
    } else {
      cloneCart[index].soLuong++;
    }

    setCart(cloneCart);
  };

  // handleSoLuong
  let handleSoLuong = (product, number) => {
    let cloneCart = [...cart];
    let index = cloneCart.findIndex((item) => {
      return item.id == product.id;
    });

    cloneCart[index].soLuong += number;

    if (cloneCart[index].soLuong == 0) {
      cloneCart.splice(index, 1);
    }

    setCart(cloneCart);
  };

  // handleRemoveFromCart
  let handleRemoveFromCart = (product) => {
    let cloneCart = [...cart];
    let index = cloneCart.findIndex((item) => {
      return item.id == product.id;
    });

    cloneCart.splice(index, 1);

    setCart(cloneCart);
  };

  // handleModal
  let handleModal = (id) => {
    let index = listShoe.findIndex((item) => {
      return item.id == id;
    });
    let cloneShoe = listShoe[index];

    setContent(cloneShoe);
  };

  return (
    <div>
      <h2 className="text-center">Ex_Shoe_Shop_Hook</h2>
      <Cart
        cart={cart}
        content={content}
        handleSoLuong={handleSoLuong}
        handleRemoveFromCart={handleRemoveFromCart}
        handleModal={handleModal}
      />
      <ListShoe handleAddToCart={handleAddToCart} listShoe={listShoe} handleModal={handleModal} />
      <Modal key={content.id} content={content}/>
    </div>
  );
}
