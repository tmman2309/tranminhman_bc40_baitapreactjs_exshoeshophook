import React from "react";

export default function Cart({
  cart,
  handleSoLuong,
  handleRemoveFromCart,
  handleModal,
}) {
  let renderBody = () => {
    return cart.map((item) => {
      return (
        <tr>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>
            <button
              onClick={() => {
                handleSoLuong(item, -1);
              }}
              className="btn btn-danger"
            >
              -
            </button>
            <strong className="mx-3">{item.soLuong}</strong>
            <button
              onClick={() => {
                handleSoLuong(item, 1);
              }}
              className="btn btn-success"
            >
              +
            </button>
          </td>
          <td>{item.price * item.soLuong}</td>
          <td>
            <img style={{ width: "80px" }} src={item.image} alt="" />
          </td>
          <td>
            <button
              onClick={() => {
                handleRemoveFromCart(item);
              }}
              className="btn btn-danger mr-2"
            >
              Xóa
            </button>
            <button
              onClick={() => {
                handleModal(item.id);
              }}
              className="btn btn-info"
              data-toggle="modal"
              data-target="#exampleModal"
            >
              Xem
            </button>
          </td>
        </tr>
      );
    });
  };

  return (
    <div>
      <table className="table">
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Quantity</th>
            <th>Price</th>
            <th>Img</th>
            <th>Thao Tác</th>
          </tr>
        </thead>
        <tbody>{renderBody()}</tbody>
      </table>
    </div>
  );
}
